#version 430 core

in vec2 TexCoord;
in vec3 fragNormal;
in vec3 fragVert;

out vec4 FragColor;

uniform vec3 camPos;
uniform vec3 lightPos;
uniform mat4 model;
uniform sampler2D ourTexture;
uniform vec3 lightColor;
uniform vec4 vertexColor;
uniform float specularity;
uniform float seasonTime;
uniform bool hardBoundaries;

void main() {
	// Invisible if alpha is low
	vec4 tex = texture(ourTexture, TexCoord) * vertexColor;
	if(tex.a < 0.1)
		discard;

	// Ambient light, average of white light and lightColor. So it's not completely dark at night, but
	// sunlight doesn't reflect off the terrain at night either.
	float ambientStrength = 0.35;
	vec3 ambient = ((ambientStrength * vec3(1.0, 1.0, 1.0)) + (ambientStrength * lightColor)) / 2.0;


	// Diffuse
	mat3 normalMatrix = transpose(inverse(mat3(model)));
	vec3 normal = normalize(normalMatrix * fragNormal);
	
	vec3 fragPosition = vec3(model * vec4(fragVert, 1.0));
	vec3 surfaceToLight = normalize(lightPos - fragPosition);

	float brightness = max(dot(normal, surfaceToLight), 0.0);
	vec3 diffuse = brightness * lightColor;

	//	Heights, used to differentiate between biomes. Water rises in spring, and snow covers more in winter.
	float minHeight = -17.5f;
	float maxHeight = -8.5f;
	float midHeight = (minHeight + maxHeight) / 2.0f;
	minHeight += max(sin(seasonTime), 0.0);
	maxHeight -= max(cos(seasonTime), 0.0);

	// specular
	vec3 viewDir = normalize(camPos - fragVert);
	vec3 reflectDir = reflect(-surfaceToLight, normal);
	float spec = 0.0;
	vec3 halfwayDir = normalize(surfaceToLight + viewDir);  
	//	If the vertex is low enough to be water or high enough to be snow, it should reflect more light.
	//	It's like an if check, but it's not, so it's better for performance!
	spec = pow(max(dot(normal, halfwayDir), 0.0), specularity - 15.0 * max(int(fragPosition.y <= minHeight), int(fragPosition.y > maxHeight)));
	vec3 specular = spec * lightColor;    
	
	float dist = distance(lightPos, fragVert);
	float attenuation = 1.0f / (1.0 + (0.03125 * dist) + (0.0 * dist * dist));

	// final colors
	vec4 surfaceColor = texture(ourTexture, TexCoord) * vertexColor;

	vec4 waterColor = vec4(0.1f, 0.2f, 0.85f, 1.0f);
	vec4 plantsColor = vec4(0.4f - (min(sin(seasonTime), 0.0f) / 4.0f), 1.0f + (min(sin(seasonTime), 0.0f) / 2.0f), 0.3f, 1.0f);
	vec4 rockColor = vec4(0.8f, 0.65f, 0.2f, 1.0f);
	vec4 snowColor = vec4(1.0f, 1.0f, 1.0f, 1.0f);

	//	Gotta untangle this mess, use math instead of if checks
	if (fragPosition.y <= minHeight)
	{
		surfaceColor = waterColor;
	}
	else if (fragPosition.y > minHeight && fragPosition.y <= midHeight)
	{
		float localHeight = (fragPosition.y - minHeight) / (midHeight - minHeight);
		if (!hardBoundaries && localHeight > 0.5f)
		{
			surfaceColor = (2.0f - localHeight * 2.0f) * plantsColor + (localHeight * 2.0f - 1.0f) * rockColor;
		}
		else surfaceColor = plantsColor;
	}
	else if (fragPosition.y > midHeight && fragPosition.y <= maxHeight)
	{
		float localHeight = (fragPosition.y - midHeight) / (maxHeight - midHeight);
		if (!hardBoundaries && localHeight > 0.5f)
		{
			surfaceColor = (2.0f - localHeight * 2.0f) * rockColor + (localHeight * 2.0f - 1.0f) * snowColor;
		}
		else surfaceColor = rockColor;
	}
	else if (fragPosition.y > maxHeight)
	{
		surfaceColor = snowColor;
	}

	//	Attempt at math instead of if checks
	/*float localHeight =
		(((fragPosition.y - minHeight) / (midHeight - minHeight)) * float(min(int(fragPosition.y > minHeight), int(fragPosition.y <= midHeight)))) +
		(((fragPosition.y - midHeight) / (maxHeight - midHeight)) * float(min(int(fragPosition.y > midHeight), int(fragPosition.y <= maxHeight))));

	bool blendColors = (!hardBoundaries && localHeight > 0.5);

	surfaceColor = 
		(waterColor * float(int(fragPosition.y <= minHeight))) +
		(plantsColor * float(min(int(fragPosition.y > minHeight), int(fragPosition.y <= midHeight)))) +
		(rockColor * float(min(int(fragPosition.y > midHeight), int(fragPosition.y <= maxHeight)))) +
		(snowColor * float(int(fragPosition.y > maxHeight)));*/

	FragColor = vec4((ambient + diffuse * attenuation) * surfaceColor.rgb + specular * attenuation, 1);
}