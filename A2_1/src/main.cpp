#include "src/main.h"

using namespace imt2531;
using namespace glm;


class game : public App 
{
public:

	game() : App(keyboardInput) { init(); }	//	Initialize game

	void init()	//	Initialize game
	{
		loadTextures();			//	Load textures
		loadObjs();				//	Load .obj files
		pacmanHandler.Init();	//	Initialize the handler for Pacman
		ghostHandler.Init();	//	Initialize the handler for Ghosts
		fruitHandler.Init();	//	Initialize the handler for Fruits
        loadLevelFromFile(0);
		loadText();				//	Load text

		lastFrame = glfwGetTime();	//	Set time of last frame

		glActiveTexture(GL_TEXTURE0);	//	
		SDL_Init(SDL_INIT_AUDIO);		//	Initialize SDL Audio
		InitAudio();					//	Initialize the Audio Manager
		SetAudioChannels(4);			//	Set Audio Channels

		bool dynamic_load = false;		//	Whether BGM should be loaded dynamically
		AddBGM("../audio/pacman_theme.wav", "pacman_theme", dynamic_load);	
		AddBGM("../audio/pacman_theme2.wav", "pacman_theme2", dynamic_load);
		//	Game SFX
		AddEffect("../audio/pacman_waka1.wav", "pacman_waka1");	
		AddEffect("../audio/pacman_waka2.wav", "pacman_waka2");	
		AddEffect("../audio/pacman_eatfruit.wav", "pacman_eatfruit");
		AddEffect("../audio/pacman_eatghost.wav", "pacman_eatghost");
		AddEffect("../audio/pacman_powerup.wav", "pacman_powerup");
		AddEffect("../audio/pacman_death.wav", "pacman_death");
		//	Menu SFX
		AddEffect("../audio/sfx1.wav", "sfx1");
		AddEffect("../audio/sfx2.wav", "sfx3");
		AddEffect("../audio/sfx2.wav", "sfx3");

		PlayBGM("pacman_theme", -1);	//	Play music, loop indefinetly

        screenY = mWindow.height();		//	Set Screen size variable
        screenX = mWindow.width();		//	Set Screen size variable
	}

	void update()
	{
		timing();	//	Update time related variables

		//--------------
		// Game        |
		//--------------

		//	Check for bonus life
		if (!bonusLife && score >= 10000)	//	Whether or not the player has gotten the bonus life
		{
			bonusLife = true;	//	Set boolean
			lives++;			//	Increase lives
		}

		//	Gameplay loop
		if (!pause && !mainMenu && !levelSelect && !highScoreMenu)
		{
			SetBGMVolume(100);	//	Music volume

			//	Set speeds
			pacmanSpeed = .1f * (1.0f + (1.0f - 1.0f / (float)level) / 2.0f);	//	Pacman
			ghostSpeed = .09f * (1.0f + (1.0f - 1.0f / (float)level) / 2.0f);	//	Ghosts

			
			if (pelletsActive <= 0)	//	If all pellets have been eaten
				resetLevel();		//	Reset the level

			//Update Pacman and Ghosts
			pacmanHandler.Update();	//	Update and draw Pacman
			ghostHandler.update();	//	Update and draw Ghosts

			//Update Fruit
			fruitHandler.update();	//	Update and draw Fruit
		}
		else
			SetBGMVolume(50);	//	Menu music volume

		//	Highscore
		if (score > highScore)	//	
			highScore = score;	//	Set HighScore

		//--------------
		// Drawing     |
		//--------------

		if (pause)				//	Pause menu
			drawMenu();
		else if (mainMenu)		//	Main Menu
			drawMainMenu();
		else if (highScoreMenu)	//	HighScore Menu
			drawHighScoreMenu();
		else if (levelSelect)	//	LevelSelect Menu
			drawLevelSelect();
		else					// Draw the game
		{
			pelletHandler.draw();	//	Draw Pellets

			for (Model w : wallModels)	//	Draw walls
			{
				w.sprite[0]->setCamera(cameraFocus, cameraPosition);	//	Set relative to camera
				w.sprite[0]->draw();									//	Draw object
			}

            drawUI();	//	Draw the in-game UI
		}

		if (windowClosed)	//	If the game is quitting
		{
			glfwDestroyWindow(mWindow.window);	//	Destroy Window
			glfwTerminate();					//	Stop GLFW
			SDL_Quit();							//	Stop SDL
			stop();								//	Stop the game loop
		}
	}

	void timing()	//	Updates time related variables
	{
		//	Timing
		float currentTime = glfwGetTime();	//	Set current time
		deltaTime = currentTime - lastFrame;//	DeltaTime	
		lastFrame = currentTime;			//	Set previous frame (which is currently this frame)
		animTimer += deltaTime;				//	Increase the animation timer with DeltaTime
	}
};

int main()			//	Main 
{
	game Mygame;	//	Create game object
	Mygame.start();	//	Start and loop the game object

	return 0;		//	Terminate program
}
