#include "Functions.h"
#include "Globals.h"
#include "objectLoader.hpp"
#include <iostream>
#include <fstream>
#include "glm/glm/glm.hpp"
#include "gl_shader.hpp"
#include "Model.h"
#include "Audio.h"
#include "makeWall.hpp"

using namespace glm;

void loadTextures()
{
	//	Load Shaders
	ShaderID = new Shader("../include/vertex.vert", "../include/fragment.frag");

	//	Load Textures
	tBrickWall = new Texture("../resources/BrickWall.jpg");
	tYellow = new Texture("../resources/yellow.png");
	tWhite = new Texture("../resources/white.png");
	tGrey = new Texture("../resources/grey.png");
	tBlue = new Texture("../resources/blue.png");
	tOranje = new Texture("../resources/oranje.png");
	tPacMan = new Texture("../resources/pacman.png");
	tGhosts.push_back(new Texture("../resources/Blinky.png"));
	tGhosts.push_back(new Texture("../resources/Pinky.png"));
	tGhosts.push_back(new Texture("../resources/Inky.png"));
	tGhosts.push_back(new Texture("../resources/Clyde.png"));
	tScaredBlue = new Texture("../resources/ScaredBlue.png");
	tScaredWhite = new Texture("../resources/ScaredWhite.png");
	tEyes = new Texture("../resources/Eyes.png");
	tFruits.push_back(new Texture("../resources/cherry.png"));
	tFruits.push_back(new Texture("../resources/strawberry.png"));
	tFruits.push_back(new Texture("../resources/orange.png"));
	tFruits.push_back(new Texture("../resources/apple.png"));
	tFruits.push_back(new Texture("../resources/watermelon.png"));
	tFruits.push_back(new Texture("../resources/cherry.png"));
	tFruits.push_back(new Texture("../resources/strawberry.png"));
	tFruits.push_back(new Texture("../resources/orange.png"));

	//	Load text
    tAscii.resize(95);
    for (int i = 0; i < 95; i++)
    {
        std::string txtPNG = "../resources/ascii/";
        txtPNG.append(std::to_string(i+32));
        txtPNG.append(".png");
        tAscii[i] = new Texture(txtPNG.c_str());
    }
}

void loadObjs()
{
	//	Load OBJs from file
	pelletObj.push_back(new Object(loadObject("../resources/pellet", OBJ)[0]));
	ghostObj.push_back(new Object(loadObject("../resources/Ghost", OBJ)[0]));
	pacmanObj.push_back(new Object(loadObject("../resources/PacMan0", OBJ)[0]));
	pacmanObj.push_back(new Object(loadObject("../resources/PacMan1", OBJ)[0]));
	pacmanObj.push_back(new Object(loadObject("../resources/PacMan2", OBJ)[0]));
	pacmanObj.push_back(new Object(loadObject("../resources/PacMan3", OBJ)[0]));
	pacmanObj.push_back(new Object(loadObject("../resources/PacMan4", OBJ)[0]));
	fruitObj.push_back(new Object(loadObject("../resources/cherry", OBJ)[0]));
    fruitObj.push_back(new Object(loadObject("../resources/strawberry", OBJ)[0]));
    fruitObj.push_back(new Object(loadObject("../resources/orange", OBJ)[0]));
    fruitObj.push_back(new Object(loadObject("../resources/apple", OBJ)[0]));
    fruitObj.push_back(new Object(loadObject("../resources/watermelon", OBJ)[0]));
	fruitObj.push_back(new Object(loadObject("../resources/cherry", OBJ)[0]));
	fruitObj.push_back(new Object(loadObject("../resources/strawberry", OBJ)[0]));
	fruitObj.push_back(new Object(loadObject("../resources/orange", OBJ)[0]));
	cube.push_back(new Object(cubeVertices, cubeNormals, cubeTextCoord, ""));
}

void loadLevelFromFile(int level)
{
	//	Find file
	std::string name = "level" + std::to_string(level);
	std::ifstream inFile;
	inFile.open("../levels/" + name);

	// Read from file
	if (inFile)
	{
		//	Get map dimensions
		std::string line;

		std::getline(inFile, line);
		std::string h = line.substr(line.find("x") + 1);
		std::string w = line.erase(line.find("x"));
		std::cout << w + " x " + h << std::endl;

		int width = std::stoi(w);
		int height = std::stoi(h);

		//	Set dimensions
		screenX = width * spriteSize;
		screenY = height * spriteSize;

		//	Set up sizes
		levelData.resize(width);
		for (int i = 0; i < width; ++i)
			levelData[i].resize(height);

		//	Read remaining map data
		int temp;
		int x = 0;
		int y = 0;
		while (inFile >> temp)
		{
			levelData[x][y] = temp;
			//std::cout << levelData[x][y] << " ";
			x++;
			if (x == width)
			{
				x -= width;
				y++;
			}
			if (y == height)
				break;
		}

		// Level loaded, close file
		std::cout << "\nLevel loaded...\n";
		inFile.close();
	}
	else
		std::cout << "Could not load file '" << name.c_str() << "'!" << std::endl;
}

void loadText()
{
	//	Create flat surface
    std::vector<GLfloat> vert =
    {
        -0.5f, -0.5f, 0.5f,
        +0.5f, -0.5f, 0.5f,
        +0.5f, +0.5f, 0.5f,
        +0.5f, +0.5f, 0.5f,
        -0.5f, +0.5f, 0.5f,
        -0.5f, -0.5f, 0.5f,
    };
    std::vector<GLfloat> norm =
    {
        -0.5f, -0.5f,  0.5f,
        0.5f, -0.5f,  0.5f,
        0.5f,  0.5f,  0.5f,
        0.5f,  0.5f,  0.5f,
        -0.5f,  0.5f,  0.5f,
        -0.5f, -0.5f,  0.5f,
    };
    std::vector<GLfloat> textCoord =
	{
		0.0f, 1.0f,
		1.0f, 1.0f,
		1.0f, 0.0f,
		1.0f, 0.0f,
		0.0f, 0.0f,
		0.0f, 1.0f,
	};
    std::vector<Object*> t;
    t.push_back(new Object(vert, norm, textCoord, ""));
	//	Create models
    ascii.resize(95);
    for (int i = 0; i < 95; i++)
    {
        Model model(ShaderID, tAscii[i], t,
            glm::vec3(0, 0, 0),
            glm::vec3(1, 1, 1),
            glm::vec3(0, 0, 0));
        ascii[i] = model;
    }
}

void initLevel()
{
	//	Load level
	loadLevelFromFile(selectedLevel);
	//std::cout << "x:" << screenX << " y:" << screenY << '\n';

    wallModels.resize(0);

	//Create wall models
	for (int i = 0; i < levelData.size(); i++)
		for (int j = 0; j < levelData[i].size(); j++)
		{
			if (levelData[i][j] == 1 || levelData[i][j] == 6)
			{
				bool left = (i != 0) ? (levelData[(i - 1)][j] == 1 || levelData[(i - 1)][j] == 6) : 0;
				bool right = (i != levelData.size() - 1) ? (levelData[(i + 1)][j] == 1 || levelData[(i + 1)][j] == 6) : 0;
				bool up = (j != 0) ? (levelData[i][(j - 1)] == 1 || levelData[i][(j - 1)] == 6) : 0;
				bool down = (j != levelData[i].size() - 1) ? (levelData[i][(j + 1)] == 1 || levelData[i][(j + 1)] == 6) : 0;

				int wallState = left * 8 + right * 4 + up * 2 + down;

				makeWall(i, j, wallState, (levelData[i][j] == 6) ? tOranje:tBlue);
			}
		}

	//	Set pellets
	setPellets();

	//	UI elements
	for (int i = 0; i < 2; i++)
	{
		Model lifeModel(ShaderID, tYellow, pacmanObj,
			glm::vec3(0, 0, 0),
			glm::vec3(0, 0, 0),
			glm::vec3(0, 0, 0));
		uiLife.push_back(lifeModel);
		Model fruitModel(ShaderID, tCherry, fruitObj,
			glm::vec3(0, 0, 0),
			glm::vec3(0, 0, 0),
			glm::vec3(0, 0, 0));
		uiFruit.push_back(fruitModel);
	}
}

void drawUI()
{
	int sprite = (level) / 2;
	if (sprite > tFruits.size()-1)
		sprite = tFruits.size()-1;

	//	Draw UI icons
	for (int i = 0; i < lives; i++)	//	Lives
	{
		uiLife[i].setVertices(2,
			glm::vec3(-((float)(levelData.size()) / 2) + i*2, ((float)(levelData[0].size()) / 2) - (levelData[0].size() - 1), +3.0) / 15.0f,
			glm::vec3(.75f, .75f, .75f) / 15.0f,
			glm::vec3(0, 0, 0));
		uiLife[i].sprite[2]->setCamera(glm::vec3(0, 0, 0), glm::vec3(0, -.1f, 35.0f) / 15.0f);
		uiLife[i].sprite[2]->draw();
	}
	for (int i = 0; i < fruitEaten; i++)	//	Fruits Eaten
	{
		uiFruit[i].sprite[sprite]->setTexture(tFruits[sprite]);
		uiFruit[i].setVertices(sprite,
			glm::vec3(-((float)(levelData.size()) / 2) + (levelData.size() - 1) - i*2, ((float)(levelData[0].size()) / 2) - (levelData[0].size() - 1), +3.0) / 15.0f,
			glm::vec3(1, 1, 1) / 15.0f,
			glm::vec3(0, 0, 0));
		uiFruit[i].sprite[sprite]->setCamera(glm::vec3(0, 0, 0), glm::vec3(0, -.1f, 35.0f) / 15.0f);
		uiFruit[i].sprite[sprite]->draw();
	}

	//	Draw UI text
	drawText(1, 1, std::to_string(score));					//	Score
	std::string high = "HIGH " + std::to_string(highScore);
	drawText((levelData.size()-1) - high.length(), 1, high);//	Highscore
	drawText((levelData.size()-1)/2 - 3, levelData[0].size()-1, "LEVEL " + std::to_string(level));	//	Level
}

void drawMenu()
{
	std::string pauseText = "Game Paused";
	drawText((levelData.size()-1)/2 - pauseText.length()/2 + 1, (levelData[0].size()-1)/3, pauseText);

	std::string resumeText = (menuChoice == 0)?"> Resume":"  Resume";
	drawText((levelData.size()-1)/2 - resumeText.length()/2, (levelData[0].size()-1)*5/10, resumeText);

	std::string quitText = (menuChoice == 1)?"> Exit  ":"  Exit  ";
	drawText((levelData.size()-1)/2 - quitText.length()/2, (levelData[0].size()-1)*6/10, quitText);
}

void drawMainMenu()
{
    std::string pacmanText = "Pacman";
    drawText((levelData.size() - 1) / 2 - pacmanText.length() / 2 + 1, (levelData[0].size() - 1) / 5, pacmanText);

    std::string startText = (mainMenuChoice == 0) ? "> Start    " : "  Start    ";
    drawText((levelData.size() - 1) / 2 - startText.length() / 2, (levelData[0].size() - 1) * 4 / 10, startText);

    std::string levelText = (mainMenuChoice == 1) ? "> Level    " : "  Level    ";
    drawText((levelData.size() - 1) / 2 - levelText.length() / 2, (levelData[0].size() - 1) * 5 / 10, levelText);

    std::string highscoreText = (mainMenuChoice == 2) ? "> Highscore" : "  Highscore";
    drawText((levelData.size() - 1) / 2 - highscoreText.length() / 2, (levelData[0].size() - 1) * 6 / 10, highscoreText);

    std::string quitText = (mainMenuChoice == 3) ? "> Quit     " : "  Quit     ";
    drawText((levelData.size() - 1) / 2 - quitText.length() / 2, (levelData[0].size() - 1) * 7 / 10, quitText);
}

void drawHighScoreMenu()
{
    std::string HighScoreText = "HighScores";
    drawText((levelData.size() - 1) / 2 - HighScoreText.length() / 2 + 1, (levelData[0].size() - 1) / 5, HighScoreText);

    std::string level1Text = "Level 1: " + std::to_string(loadHighScore(0));
    drawText((levelData.size() - 1) / 2 - level1Text.length() / 2, (levelData[0].size() - 1) * 4 / 10, level1Text);

    std::string level2Text = "Level 2: " + std::to_string(loadHighScore(1));
    drawText((levelData.size() - 1) / 2 - level2Text.length() / 2, (levelData[0].size() - 1) * 5 / 10, level2Text);

    std::string level3Text = "Level 3: " + std::to_string(loadHighScore(2));
    drawText((levelData.size() - 1) / 2 - level3Text.length() / 2, (levelData[0].size() - 1) * 6 / 10, level3Text);

    std::string quitText = "> Quit";
    drawText((levelData.size() - 1) / 2 - quitText.length() / 2, (levelData[0].size() - 1) * 7 / 10, quitText);
}

void drawLevelSelect()
{
    std::string LevelText = "Level Select";
    drawText((levelData.size() - 1) / 2 - LevelText.length() / 2 + 1, (levelData[0].size() - 1) / 5, LevelText);

    std::string level1Text = (selectedLevel == 0) ? "> Level 1" : "  Level 1";
    drawText((levelData.size() - 1) / 2 - level1Text.length() / 2, (levelData[0].size() - 1) * 4 / 10, level1Text);

    std::string level2Text = (selectedLevel == 1) ? "> Level 2" : "  Level 2";
    drawText((levelData.size() - 1) / 2 - level2Text.length() / 2, (levelData[0].size() - 1) * 5 / 10, level2Text);

    std::string level3Text = (selectedLevel == 2) ? "> Level 3" : "  Level 3";
    drawText((levelData.size() - 1) / 2 - level3Text.length() / 2, (levelData[0].size() - 1) * 6 / 10, level3Text);
}

void drawText(int x, int y, std::string s)
{
	int i = 0;
	for (const auto c : s)
	{
        int n = (int)c - 32;
        ascii[n].setVertices(0,
            glm::vec3((-((float)(levelData.size()) / 2) + x + i++), (((float)(levelData[0].size()) / 2) - y), +3.0f) / 15.0f,
            glm::vec3(1, 1, 1)/15.0f,
            glm::vec3(0, 0, 0));
        ascii[(int)c - 32].sprite[0]->setCamera(glm::vec3(0, 0, 0), glm::vec3(0, -.1f, 35.0f) / 15.0f);
        ascii[(int)c - 32].sprite[0]->draw();
        //delete model.sprite;
	}
}

void keyboardInput(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	//	Reset
	if (key == GLFW_KEY_R && action == GLFW_RELEASE)
	{
		resetGame();
		PlayEffect("sfx3");
	}

	//	Filthy hax
	if (key == GLFW_KEY_P && action == GLFW_RELEASE)
	{
		resetLevel();
	}

	//	Pause
	if (!highScoreMenu && !levelSelect)
		if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE && !(mainMenu || levelSelect))
		{
			pause = !pause;
			PlayEffect("sfx3");
		}

    if (pause)
    {
        //	Menu
		if ((key == GLFW_KEY_S || key == GLFW_KEY_DOWN) && action == GLFW_RELEASE)
		{
			menuChoice++;
			PlayEffect("sfx1");
		}
		if ((key == GLFW_KEY_W || key == GLFW_KEY_UP) && action == GLFW_RELEASE)
		{
			menuChoice--;
			PlayEffect("sfx1");
		}

        menuChoice = clamp(menuChoice, 0, 1);

        //Select menu choice
        if ((key == GLFW_KEY_SPACE || key == GLFW_KEY_ENTER) && action == GLFW_RELEASE)
        {
			PlayEffect("sfx3");
            if (menuChoice == 0)
                pause = false;
            if (menuChoice == 1)
                exitGame();
        }
    }
    else if (mainMenu)
    {
        //  Menu
		if ((key == GLFW_KEY_S || key == GLFW_KEY_DOWN) && action == GLFW_RELEASE)
		{
			mainMenuChoice++;
			PlayEffect("sfx1");
		}
		if ((key == GLFW_KEY_W || key == GLFW_KEY_UP) && action == GLFW_RELEASE)
		{
			mainMenuChoice--;
			PlayEffect("sfx1");
		}

        mainMenuChoice = clamp(mainMenuChoice, 0, 3);

        //Select menu choice
        if ((key == GLFW_KEY_SPACE || key == GLFW_KEY_ENTER) && action == GLFW_RELEASE)
        {
			PlayEffect("sfx3");
            if (mainMenuChoice == 0)
                startGame();
            if (mainMenuChoice == 1)
            {
                mainMenu = false;
                levelSelect = true;
            }
            if (mainMenuChoice == 2)
            {
                mainMenu = false;
                highScoreMenu = true;
            }
            if (mainMenuChoice == 3)
                quitGame(window);
        }
    }
    else if (highScoreMenu)
    {
        //can only go back out from highscore menu
        if ((key == GLFW_KEY_SPACE || key == GLFW_KEY_ENTER) && action == GLFW_RELEASE)
        {
			PlayEffect("sfx3");
            mainMenu = true;
            highScoreMenu = false;
        }
    }
    else if (levelSelect)
    {
        //  Menu
		if ((key == GLFW_KEY_S || key == GLFW_KEY_DOWN) && action == GLFW_RELEASE)
		{
			selectedLevel++;
			PlayEffect("sfx1");
		}
		if ((key == GLFW_KEY_W || key == GLFW_KEY_UP) && action == GLFW_RELEASE)
		{
			selectedLevel--;
			PlayEffect("sfx1");
		}

        selectedLevel = clamp(selectedLevel, 0, 2);

        //Select menu choice
        if ((key == GLFW_KEY_SPACE || key == GLFW_KEY_ENTER) && action == GLFW_RELEASE)
        {
			PlayEffect("sfx3");
            levelSelect = false;
            mainMenu = true;
        }
    }
    else
    {
		//	Camera mode
		if (key == GLFW_KEY_SPACE && action == GLFW_PRESS)cameraMode = (cameraMode + 1) % 3;
		//	Player Move
		Direction d = None;
        if ((key == GLFW_KEY_D || key == GLFW_KEY_RIGHT) && action == GLFW_PRESS) d = Right;
        if ((key == GLFW_KEY_A || key == GLFW_KEY_LEFT) && action == GLFW_PRESS)	d = Left;
        if ((key == GLFW_KEY_S || key == GLFW_KEY_DOWN) && action == GLFW_PRESS)  d = Down;
        if ((key == GLFW_KEY_W || key == GLFW_KEY_UP) && action == GLFW_PRESS)	d = Up;
		pacmanHandler.input(d);
    }
}

bool checkDirection(Direction dir, int x, int y, bool check6)	//	Checks for collision in a given direction from position
{
    if (dir == Right)
    {
        if (x + 1 == levelData.size())
            return true;
        else if (levelData[x + 1][y] != 1 && ((check6) ? (levelData[x + 1][y] != 6) : true))
            return true;
        else
            return false;
    }
    if (dir == Left)
    {
        if (x - 1 == -1)
            return true;
        else if (levelData[x - 1][y] != 1 && ((check6) ? (levelData[x - 1][y] != 6) : true))
            return true;
        else
            return false;
    }
    if (dir == Down)
    {
        if (y + 1 == levelData[x].size() - 1)
            return true;
        else if (levelData[x][y + 1] != 1 && ((check6) ? (levelData[x][y + 1] != 6) : true))
            return true;
        else
            return false;
    }
    if (dir == Up)
    {
        if (y - 1 == 0)
            return true;
        else if (levelData[x][y - 1] != 1 && ((check6) ? (levelData[x][y - 1] != 6) : true))
            return true;
        else
            return false;
    }

    return false;
}

void resetGame()
{
	//Stop msuic
	StopBGM();
	PlayBGM("pacman_theme2", -1);

	//Reset all relevant data
    pacmanSpeed = .1f;
    ghostSpeed = .09f;

    phaseTime = 7;
    phaseTimer = 0;
    scareTime = 10;
    scareTimer = 0;
    phaseCount = 0;
    lives = 2;
	level = 0;	//	Set to 0, the first level is level 1, but in resetLevel this will be increased from 0 to 1
	score = 0;

    menuChoice = 0;

    resetLevel();	//	Reset level data
}

void resetLevel()
{
	//	Resets the level objects, this also happens when the player clears a level
	pelletHandler.reset();

	fruitPlaced[0] = false;
	fruitPlaced[1] = false;
	fruitEaten = 0;
	level++;

    resetCreatures();	//	Reset creatures
}

void resetCreatures()
{
	//	Resets all the creatures
	pacmanHandler.reset();
	ghostHandler.reset();
	ghostsEaten = 0;
	fruitHandler.reset();
}

void setPellets()
{
    //  Sets pellets to be correct for the level currently loaded
    pelletHandler.pellets.resize(0);
	pelletHandler.models.resize(0);
    //	Create Pellets
    for (int i = 0; i < levelData.size(); i++)
        for (int j = 0; j < levelData[i].size(); j++)
            if (levelData[i][j] == 2)
            {
				Model model(ShaderID, tWhite, pelletObj,
					glm::vec3(-((float)(levelData.size()) / 2) + i, ((float)(levelData[i].size()) / 2) - j, 0),
					glm::vec3(.1f, .1f, .1f),
					glm::vec3(0, 0, 0));
				pelletHandler.pellets.resize(pelletHandler.pellets.size() + 1);
				pelletHandler.pellets[pelletHandler.pellets.size() - 1] = Pellet(i, j, false);
				pelletHandler.models.push_back(model);
            }
            else if (levelData[i][j] == 3)
            {
				Model model(ShaderID, tWhite, pelletObj,
					glm::vec3(-((float)(levelData.size()) / 2) + i, ((float)(levelData[i].size()) / 2) - j, 0),
					glm::vec3(.3f, .3f, .3f),
					glm::vec3(0, 0, 0));
				pelletHandler.pellets.resize(pelletHandler.pellets.size() + 1);
				pelletHandler.pellets[pelletHandler.pellets.size() - 1] = Pellet(i, j, true);
				pelletHandler.models.push_back(model);
            }
	//	Set total and active total
    pelletTotal = pelletHandler.pellets.size();
    pelletsActive = pelletTotal;
}

void exitGame()
{
    //exit to main menu
    mainMenu = true;
    pause = false;
    resetGame();
    saveHighScore();
}

void quitGame(GLFWwindow* window)
{
    //quit game
	glfwDestroyWindow(window);
	glfwTerminate();
    windowClosed = true;
}

int loadHighScore(int lvl)
{
    //loads highscore for given level and returns it
    int score = 0;
    std::string file = "highScore" + std::to_string(lvl) + ".txt";
    std::ifstream in;
    in.open(file);
    if (in)
    {
        in >> score;
        in.close();
    }

    return score;
}

void saveHighScore()
{
    //saves current highscore to file for current level
    std::string file = "highScore" + std::to_string(selectedLevel) + ".txt";
    std::ofstream out;
    out.open(file);
    if (out)
    {
        out << highScore;
        out.close();
    }
}

void startGame()
{
    //loads level, sets the pellets and loads highscore
    mainMenu = false;
    initLevel();			//	Initialize the map
    highScore = loadHighScore(selectedLevel);
}