#pragma once
#include "Entity.h"
#include "Structs.h"

class TileMap : public Entity
{
private:
    Mesh * mesh;
    Material* material;
    Buffer* buffer;
public:
    TileMap();
    TileMap(Material* mat, Mesh* m, Buffer* buff, const char* map_path);

    void draw();
    void update(double delta_time);
};