#include "Entity.h"
#include "Structs.h"


// Ghosts in pacman

class Ghost : public Entity
{
private:
    Mesh * mesh;                // Struct with vertices, texture coordinates etc
    Buffer* buffer;             // Struct containing VAO, VBO, IBO

    bool jailed;                // If ghost is in the center
    int ID;                     // ID of the ghost, color based on this
    double elapsedTime;         // Time sat in jail

    glm::vec3 startPos;         // Original grid position
public:
    Material * material;        // Struct containing texture and shader

    bool eaten;                 // Eaten by pacman
    bool updated;               // If it's direction has been updated after collision/crossroad

    glm::vec3 direct;           // Direction ghost is facing

    Ghost();                    // Unused constructor
    Ghost(Material* mat, Mesh* m, Buffer* buff, glm::vec3 pos, int id); // Default constructor we use

    void draw();                
    void update(double delta_time);
    void changeDirection();
    void reset();
};
