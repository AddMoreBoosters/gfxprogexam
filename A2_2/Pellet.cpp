#include "Pellet.h"
#include "logger.h"
#include "mad.h"
#include "Pacman.h"

int pellets_left;

extern int tiles[36][28];

extern glm::mat4 g_view;
extern glm::vec3 camPos;

extern Pacman pacman;

// Unused constructor
Pellet::Pellet() {}

// Default constructor we use
Pellet::Pellet(Material* mat, Mesh* m, Buffer* buff, const char* map_path) : Entity()
{
    material = mat;
    mesh = m;
    buffer = buff;

    // For loop to get default values from tiles
    for (unsigned int i = 0; i < sizeof(tiles) / sizeof(tiles[0]); i++)
    {
        for (unsigned int j = 0; j < sizeof(tiles[0]) / sizeof(int); j++)
        {
            startstate[i][j] = tiles[i][j];
            if (tiles[i][j] == 3 || tiles[i][j] == 4)
                pellets_left++;
        }
    }

    startpellets = pellets_left;

    // Read map data
    std::string map_data;
    mad::readStringFromFile(map_path, map_data);

    // Mesh creation
    int x = 0;
    int y = 35;

    std::vector<glm::vec3> vert;
    std::vector<glm::vec2> tex;
    std::vector<glm::uvec3> ind;
    std::vector<glm::vec3> norm;

    // Adds a cube to the vector for every pellet or superpellet
    for (char& c : map_data)
    {
        switch (c)
        {
        case '3':
            // Normal pellets
            // Vertices //
            // Front
            vert.push_back(glm::vec3( 0.15f + x,  0.15f + y,  0.15f));     // Top Right
            vert.push_back(glm::vec3( 0.15f + x, -0.15f + y,  0.15f));     // Bottom Right
            vert.push_back(glm::vec3(-0.15f + x, -0.15f + y,  0.15f));     // Bottom Left
            vert.push_back(glm::vec3(-0.15f + x,  0.15f + y,  0.15f));     // Top Left

            // Right
            vert.push_back(glm::vec3( 0.15f + x,  0.15f + y, -0.15f));     // Top Right
            vert.push_back(glm::vec3( 0.15f + x, -0.15f + y, -0.15f));     // Bottom Right
            vert.push_back(glm::vec3( 0.15f + x, -0.15f + y,  0.15f));     // Bottom Left
            vert.push_back(glm::vec3( 0.15f + x,  0.15f + y,  0.15f));     // Top Left

            // Back
            vert.push_back(glm::vec3( 0.15f + x,  0.15f + y, -0.15f));     // Top Right
            vert.push_back(glm::vec3( 0.15f + x, -0.15f + y, -0.15f));     // Bottom Right
            vert.push_back(glm::vec3(-0.15f + x, -0.15f + y, -0.15f));     // Bottom Left
            vert.push_back(glm::vec3(-0.15f + x,  0.15f + y, -0.15f));     // Top Left

            // Left
            vert.push_back(glm::vec3(-0.15f + x,  0.15f + y,  0.15f));     // Top Right
            vert.push_back(glm::vec3(-0.15f + x, -0.15f + y,  0.15f));     // Bottom Right
            vert.push_back(glm::vec3(-0.15f + x, -0.15f + y, -0.15f));     // Bottom Left
            vert.push_back(glm::vec3(-0.15f + x,  0.15f + y, -0.15f));     // Top Left

            // Top
            vert.push_back(glm::vec3( 0.15f + x,  0.15f + y, -0.15f));     // Top Right
            vert.push_back(glm::vec3( 0.15f + x,  0.15f + y,  0.15f));     // Bottom Right
            vert.push_back(glm::vec3(-0.15f + x,  0.15f + y,  0.15f));     // Bottom Left
            vert.push_back(glm::vec3(-0.15f + x,  0.15f + y, -0.15f));     // Top Left

            // Bottom
            vert.push_back(glm::vec3( 0.15f + x, -0.15f + y,  0.15f));     // Top Right
            vert.push_back(glm::vec3( 0.15f + x, -0.15f + y, -0.15f));     // Bottom Right
            vert.push_back(glm::vec3(-0.15f + x, -0.15f + y, -0.15f));     // Bottom Left
            vert.push_back(glm::vec3(-0.15f + x, -0.15f + y,  0.15f));     // Top Left

            // Normals //
            // Front
            norm.push_back(glm::vec3(0.5773491859436035, 0.5773491859436035, 0.5773491859436035));
            norm.push_back(glm::vec3(0.5773491859436035, -0.5773491859436035, 0.5773491859436035));
            norm.push_back(glm::vec3(-0.5773491859436035, -0.5773491859436035, 0.5773491859436035));
            norm.push_back(glm::vec3(-0.5773491859436035, 0.5773491859436035, 0.5773491859436035));

            // Right
            norm.push_back(glm::vec3(0.5773491859436035, 0.5773491859436035, -0.5773491859436035));
            norm.push_back(glm::vec3(0.5773491859436035, -0.5773491859436035, -0.5773491859436035));
            norm.push_back(glm::vec3(0.5773491859436035, -0.5773491859436035, 0.5773491859436035));
            norm.push_back(glm::vec3(0.5773491859436035, 0.5773491859436035, 0.5773491859436035));

            // Back
            norm.push_back(glm::vec3(0.5773491859436035, 0.5773491859436035, -0.5773491859436035));
            norm.push_back(glm::vec3(0.5773491859436035, -0.5773491859436035, -0.5773491859436035));
            norm.push_back(glm::vec3(-0.5773491859436035, -0.5773491859436035, -0.5773491859436035));
            norm.push_back(glm::vec3(-0.5773491859436035, 0.5773491859436035, -0.5773491859436035));

            // Left
            norm.push_back(glm::vec3(-0.5773491859436035, 0.5773491859436035, 0.5773491859436035));
            norm.push_back(glm::vec3(-0.5773491859436035, -0.5773491859436035, 0.5773491859436035));
            norm.push_back(glm::vec3(-0.5773491859436035, -0.5773491859436035, -0.5773491859436035));
            norm.push_back(glm::vec3(-0.5773491859436035, 0.5773491859436035, -0.5773491859436035));

            // Top
            norm.push_back(glm::vec3(0.5773491859436035, 0.5773491859436035, -0.5773491859436035));
            norm.push_back(glm::vec3(0.5773491859436035, 0.5773491859436035, 0.5773491859436035));
            norm.push_back(glm::vec3(-0.5773491859436035, 0.5773491859436035, 0.5773491859436035));
            norm.push_back(glm::vec3(-0.5773491859436035, 0.5773491859436035, -0.5773491859436035));

            // Bottom
            norm.push_back(glm::vec3(0.5773491859436035, -0.5773491859436035, 0.5773491859436035));
            norm.push_back(glm::vec3(0.5773491859436035, -0.5773491859436035, -0.5773491859436035));
            norm.push_back(glm::vec3(-0.5773491859436035, -0.5773491859436035, -0.5773491859436035));
            norm.push_back(glm::vec3(-0.5773491859436035, -0.5773491859436035, 0.5773491859436035));

            x++;
            break;
        case '0':
            x++;
            break;
        case '\n':
            y--;
            x = 0;
            break;
        case '1':
            x++;
            break;
        case '2':
            x++;
            break;
        case '4':
            // Superpellets
            // Vertices //
            // Front
            vert.push_back(glm::vec3(0.35f + x, 0.35f + y, 0.35f));     // Top Right
            vert.push_back(glm::vec3(0.35f + x, -0.35f + y, 0.35f));     // Bottom Right
            vert.push_back(glm::vec3(-0.35f + x, -0.35f + y, 0.35f));     // Bottom Left
            vert.push_back(glm::vec3(-0.35f + x, 0.35f + y, 0.35f));     // Top Left

            // Right
            vert.push_back(glm::vec3(0.35f + x, 0.35f + y, -0.35f));     // Top Right
            vert.push_back(glm::vec3(0.35f + x, -0.35f + y, -0.35f));     // Bottom Right
            vert.push_back(glm::vec3(0.35f + x, -0.35f + y, 0.35f));     // Bottom Left
            vert.push_back(glm::vec3(0.35f + x, 0.35f + y, 0.35f));     // Top Left

            // Back
            vert.push_back(glm::vec3(0.35f + x, 0.35f + y, -0.35f));     // Top Right
            vert.push_back(glm::vec3(0.35f + x, -0.35f + y, -0.35f));     // Bottom Right
            vert.push_back(glm::vec3(-0.35f + x, -0.35f + y, -0.35f));     // Bottom Left
            vert.push_back(glm::vec3(-0.35f + x, 0.35f + y, -0.35f));     // Top Left

            // Left
            vert.push_back(glm::vec3(-0.35f + x, 0.35f + y, 0.35f));     // Top Right
            vert.push_back(glm::vec3(-0.35f + x, -0.35f + y, 0.35f));     // Bottom Right
            vert.push_back(glm::vec3(-0.35f + x, -0.35f + y, -0.35f));     // Bottom Left
            vert.push_back(glm::vec3(-0.35f + x, 0.35f + y, -0.35f));     // Top Left

            // Top
            vert.push_back(glm::vec3(0.35f + x, 0.35f + y, -0.35f));     // Top Right
            vert.push_back(glm::vec3(0.35f + x, 0.35f + y, 0.35f));     // Bottom Right
            vert.push_back(glm::vec3(-0.35f + x, 0.35f + y, 0.35f));     // Bottom Left
            vert.push_back(glm::vec3(-0.35f + x, 0.35f + y, -0.35f));     // Top Left

            // Bottom
            vert.push_back(glm::vec3(0.35f + x, -0.35f + y, 0.35f));     // Top Right
            vert.push_back(glm::vec3(0.35f + x, -0.35f + y, -0.35f));     // Bottom Right
            vert.push_back(glm::vec3(-0.35f + x, -0.35f + y, -0.35f));     // Bottom Left
            vert.push_back(glm::vec3(-0.35f + x, -0.35f + y, 0.35f));     // Top Left

            // Normals //
            // Front
            norm.push_back(glm::vec3(0.5773491859436035, 0.5773491859436035, 0.5773491859436035));
            norm.push_back(glm::vec3(0.5773491859436035, -0.5773491859436035, 0.5773491859436035));
            norm.push_back(glm::vec3(-0.5773491859436035, -0.5773491859436035, 0.5773491859436035));
            norm.push_back(glm::vec3(-0.5773491859436035, 0.5773491859436035, 0.5773491859436035));

            // Right
            norm.push_back(glm::vec3(0.5773491859436035, 0.5773491859436035, -0.5773491859436035));
            norm.push_back(glm::vec3(0.5773491859436035, -0.5773491859436035, -0.5773491859436035));
            norm.push_back(glm::vec3(0.5773491859436035, -0.5773491859436035, 0.5773491859436035));
            norm.push_back(glm::vec3(0.5773491859436035, 0.5773491859436035, 0.5773491859436035));

            // Back
            norm.push_back(glm::vec3(0.5773491859436035, 0.5773491859436035, -0.5773491859436035));
            norm.push_back(glm::vec3(0.5773491859436035, -0.5773491859436035, -0.5773491859436035));
            norm.push_back(glm::vec3(-0.5773491859436035, -0.5773491859436035, -0.5773491859436035));
            norm.push_back(glm::vec3(-0.5773491859436035, 0.5773491859436035, -0.5773491859436035));

            // Left
            norm.push_back(glm::vec3(-0.5773491859436035, 0.5773491859436035, 0.5773491859436035));
            norm.push_back(glm::vec3(-0.5773491859436035, -0.5773491859436035, 0.5773491859436035));
            norm.push_back(glm::vec3(-0.5773491859436035, -0.5773491859436035, -0.5773491859436035));
            norm.push_back(glm::vec3(-0.5773491859436035, 0.5773491859436035, -0.5773491859436035));

            // Top
            norm.push_back(glm::vec3(0.5773491859436035, 0.5773491859436035, -0.5773491859436035));
            norm.push_back(glm::vec3(0.5773491859436035, 0.5773491859436035, 0.5773491859436035));
            norm.push_back(glm::vec3(-0.5773491859436035, 0.5773491859436035, 0.5773491859436035));
            norm.push_back(glm::vec3(-0.5773491859436035, 0.5773491859436035, -0.5773491859436035));

            // Bottom
            norm.push_back(glm::vec3(0.5773491859436035, -0.5773491859436035, 0.5773491859436035));
            norm.push_back(glm::vec3(0.5773491859436035, -0.5773491859436035, -0.5773491859436035));
            norm.push_back(glm::vec3(-0.5773491859436035, -0.5773491859436035, -0.5773491859436035));
            norm.push_back(glm::vec3(-0.5773491859436035, -0.5773491859436035, 0.5773491859436035));

            x++;
            break;
        case '5':
            x++;
            break;
        }

    }

    // Storing texture coordinates
    for (size_t i = 0; i < vert.size() / 4; i++)
    {
        tex.push_back(glm::vec2(1.0f, 1.0f));
        tex.push_back(glm::vec2(1.0f, 0.0f));
        tex.push_back(glm::vec2(0.0f, 0.0f));
        tex.push_back(glm::vec2(0.0f, 1.0f));
    }

    // Storing vertices
    for (size_t i = 0; i < vert.size(); i += 4)
    {
        ind.push_back(glm::uvec3(i + 3, i + 1, i));
        ind.push_back(glm::uvec3(i + 3, i + 2, i + 1));
    }

    mesh->vertices = vert;
    mesh->texture_coordinates = tex;
    mesh->indices = ind;
    mesh->normals = norm;

    glGenVertexArrays(1, &buffer->VAO);                                         
    glBindVertexArray(buffer->VAO);

    glGenBuffers(3, &buffer->VBO[0]);                                           // Generates 3 unique ID for buffer object

    glBindBuffer(GL_ARRAY_BUFFER, buffer->VBO[0]);                              // Binds unique buffer to a target, GL_ARRAY_BUFFER here
    glBufferData(GL_ARRAY_BUFFER, (vert.size() * sizeof(glm::vec3)), vert.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);               // How to interpret the vertex data
    // Which vertex attribute, number of dimensions in the vector, type of data, normalized or not, space between attribute sets, offset

    glEnableVertexAttribArray(0); // <-------------------------------- Remember

    glBindBuffer(GL_ARRAY_BUFFER, buffer->VBO[1]);                              // Binds unique buffer to a target, GL_ARRAY_BUFFER here
    glBufferData(GL_ARRAY_BUFFER, (tex.size() * sizeof(glm::vec2)), tex.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);               // How to interpret the vertex data
    // Which vertex attribute, number of dimensions in the vector, type of data, normalized or not, space between attribute sets, offset

    glEnableVertexAttribArray(1);

    glBindBuffer(GL_ARRAY_BUFFER, buffer->VBO[2]);                              // Binds unique buffer to a target, GL_ARRAY_BUFFER here
    glBufferData(GL_ARRAY_BUFFER, (norm.size() * sizeof(glm::vec3)), norm.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);               // How to interpret the vertex data
    // Which vertex attribute, number of dimensions in the vector, type of data, normalized or not, space between attribute sets, offset

    glEnableVertexAttribArray(2);

    glGenBuffers(1, &buffer->IBO);                                          // Generates unique ID for Element object
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer->IBO);                     // Binds unique buffer to a target, GL_ELEMENT_ARRAY_BUFFER here
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh->indices.size() * sizeof(glm::uvec3), mesh->indices.data(), GL_STATIC_DRAW);
    // Copies indices data into the GL_ELEMENT_ARRAY_BUFFER target

    // Unbinds everything
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void Pellet::draw()
{
    material->shader->use();                                            // Uses bound shader program
    
    material->shader->setMatrix("model", transform.rotation);           // Sets model uniform in shader
    material->shader->setMatrix("view", g_view);                        // Sets view uniform in shader

    //glBindTexture(GL_TEXTURE_2D, material->texture->getNo());         // Unused texture
    glBindVertexArray(buffer->VAO);                                     // Binds VAO for draw
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer->IBO);                 // Binds IBO for draw
    glDrawElements(GL_TRIANGLES, mesh->indices.size() * sizeof(glm::uvec3), GL_UNSIGNED_INT, 0); // Draws elements
}

void Pellet::update(double delta_time)
{
    // Sets light position uniform in shader
    material->shader->setFloat("lightPos", pacman.transform.position.x, pacman.transform.position.y, 5.0f);
    // Sets camera position uniform in shader
    material->shader->setFloat("camPos", camPos.x, camPos.y, camPos.z);
    // Sets tiles uniform in shader
    material->shader->setInt("tiles", tiles);
}

void Pellet::reset()
{
    // Resets to default values
    for (unsigned int i = 0; i < sizeof(tiles) / sizeof(tiles[0]); i++)
    {
        for (unsigned int j = 0; j < sizeof(tiles[0]) / sizeof(int); j++)
        {
            tiles[i][j] = startstate[i][j];
        }
    }
    pellets_left = startpellets;
}
