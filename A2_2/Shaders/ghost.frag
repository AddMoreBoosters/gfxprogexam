#version 430 core

in vec3 vertexColor;
in vec2 TexCoord;
in vec3 fragNormal;
in vec3 fragVert;


out vec4 FragColor;

uniform vec3 camPos;
uniform vec3 lightPos;
uniform mat4 model;
uniform vec4 ourColor;
uniform sampler2D ourTexture;

void main() {
	vec3 lightColor = vec3(1.0, 1.0, 1.0);

	// Ambient
	float ambientStrength = 0.2;
	vec3 ambient = ambientStrength * lightColor;


	// Diffuse
	mat3 normalMatrix = transpose(inverse(mat3(model)));
	vec3 normal = normalize(normalMatrix * fragNormal);
	
	vec3 fragPosition = vec3(model * vec4(fragVert, 1.0));
	vec3 surfaceToLight = normalize(lightPos - fragPosition);

	float brightness = max(dot(normal, surfaceToLight), 0.0);
	vec3 diffuse = brightness * lightColor;


	// Specular
	float specularStrength = 1.0;
    vec3 viewDir = normalize(camPos - fragVert);
    vec3 reflectDir = reflect(-surfaceToLight, normalize(fragNormal));  
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), 4);
    vec3 specular = specularStrength * spec * lightColor; 


	float dist = distance(lightPos, fragVert);
	float attenuation = 1.0f / (1.0 + (0.03125 * dist) + (0.0 * dist * dist));

	vec4 surfaceColor = vec4(vertexColor, 1.0);
	
	FragColor = vec4((ambient + diffuse + specular * attenuation) * surfaceColor.rgb, surfaceColor.a);
}