#pragma once
#include <vector>

#include "Shaders.h"
#include "Texture.h"

#ifdef _WIN32
#include "Dependencies\glew-2.1.0\include\GL\glew.h"
#include "Dependencies\glm\glm\glm.hpp"
#include "Dependencies\glm\glm\gtc\matrix_transform.hpp"
#else
#include <GL/glew.h>
#include "glm/glm.hpp"
#include <glm/gtc/matrix_transform.hpp>
#endif

// Container for pointers to shader and texture, 
// Different materials may contain different combinations of shaders/textures
struct Material
{
    Shader* shader;
    Texture* texture;
};

// Container for different mesh attributes
struct Mesh
{
    std::vector<glm::vec3> vertices;                // tinyobj -> attrib_t.vertices
    std::vector<glm::vec2> texture_coordinates;     // tinyobj -> attrib_t.texcoords
    std::vector<glm::uvec3> indices;                // tinyobj -> shape_t.mesh_t.indices.vertex_index
    std::vector<glm::vec3> normals;                  // tinyobj -> attrib_t.normals
};

// Container with functions for position and rotation
struct Transform
{
    glm::vec3 position;
    glm::mat4 rotation;
    float speed;
    void rotate(float angle, glm::vec3 direction, double delta_time)
    {
        rotation = glm::rotate(rotation, glm::radians(angle) * (float) delta_time, direction);
    }
    void translate(glm::vec3 direction, double delta_time)
    {
        position += (direction * (float)delta_time * speed);
    }
};

// Container for different buffer objects
struct Buffer
{
    GLuint VAO;
    GLuint VBO[3];
    GLuint IBO;
};