#pragma once
#include "GameObject.h"
#include "Enums.h"

#include <SDL2_ttf/include/SDL_ttf.h>


class TextBox : public GameObject
{
private:
    TTF_Font* font;
public:
    TextBox(TTF_Font* fnt, std::string text, SDL_Color color, glm::vec3 pos, Allignment allign);
};