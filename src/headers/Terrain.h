#pragma once
#include "GameObject.h"

class Terrain : public GameObject
{
public:
	Terrain();

	virtual void update();
	virtual void input(int key, int scancode, int action, int mods);

private:
	bool hardBoundaries;
	bool seasonsPaused;
	float timeSpeed;
	float seasonTime;
};
