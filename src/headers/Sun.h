#pragma once
#include "Movable.h"

class Sun : public Movable
{
public:
	Sun();
	~Sun();

	virtual void update();
	virtual void input(int key, int scancode, int action, int mods);

private:
	Light * light;
	bool moveAuto;
	float orbitRadius;
	float timeOfDay;
	float timeSpeed;
};