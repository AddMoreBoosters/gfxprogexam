#include "src/headers/Functions.h"
#include "src/headers/ObjectHandler.h"
#include "src/headers/Init.h"
#include "src/headers/Globals.h"
#include "src/headers/Camera.h"

#include "src/headers/Movable.h"
#include "src/headers/Sun.h"
#include "src/headers/Terrain.h"
#include "src/headers/TextBox.h"

#define AUDIO_IMPLEMENTATION
#include "src/headers/Audio.hpp"

Sun* sun;
Movable* glider;
Mesh* gliderMesh;
Mesh* sunMesh;
//TextBox* text;

//--------------------------------------------------------------------
//	USED TO INITIALIZE THE GAME AND YOUR GAMEOBJECTS, CLASSES, AND FUNCTIONS
//--------------------------------------------------------------------
void Start()
{
	initTextures();
	initShaders();
	initMeshes();
	initGameObjects();
    Audio::InitAudio();

    //Audio::AddBGM("../resources/sound/pacman_jazz.wav", "bgm", false);
	Audio::AddBGM("../resources/sound/victory.wav", "bgm", false);

	glider = new Movable();
	gliderMesh = LoadObject("glider");
	Texture* tex = LoadTexture("texture");
	Shader* standardShader = LoadShader("standard");
	glider->transform.scale = glm::vec3(0.3f);			//	The model is quite big, so I scale it down

	sun = new Sun();
	sunMesh = LoadObject("watermelon");
	Texture* sunTex = LoadTexture("watermelon");
	
	Terrain* terrain = new Terrain();
	Mesh* height = LoadObject("terrain");
	Texture* texTerrain = LoadTexture("white");
	Shader* terrainShader = LoadShader("terrain");
	InitGameObject(terrain, height, texTerrain, terrainShader);
	terrain->transform.scale = glm::vec3(100.0f);		//	The model is tiny so I scale it up

    mainCamera = new Camera(glm::vec3(0, 10, 5), glm::vec3(0, 0, -1), glm::vec3(0, 1, 0), followCam);

	//Shader* textShader = LoadShader("text");

	//TTF_Font* font = TTF_OpenFont("../resources/fonts/Joystix.TTF", 20);     // Loads the font

	//SDL_Color white = { 255,0,255,255 };
	//text = new TextBox(font, "test", white, glm::vec3(1.0f), left);

	InitGameObject(glider, gliderMesh, tex, standardShader);
	InitGameObject(sun, sunMesh, sunTex, standardShader);
    InitGameObject(mainCamera, nullptr, nullptr, nullptr);
	//InitGameObject(text, text->mesh, text->material.texture, textShader);

    mainCamera->setTarget(glider);
	mainCamera->setFollowDistance(glm::vec3(0, 5, 5));
	mainCamera->setLookatPosition(glm::vec3(0, 0, 0));

    //Audio::PlayBGM("bgm", -1);
}

//--------------------------------------------------------------------
//	USED TO UPDATE THE GAME. CALLED ONCE PER FRAME.
//--------------------------------------------------------------------
void Update()
{
	
}