#include "src/headers/Terrain.h"
#include "src/headers/Globals.h"

Terrain::Terrain()
{
	hardBoundaries = false;
	seasonsPaused = false;
	timeSpeed = 360.0f / 30.0f;			//	360 degrees, divided by how many seconds I want a cycle to take
	seasonTime = 0.0f;
}

void Terrain::update()
{
	if (!seasonsPaused)
	{
		seasonTime += (float)g_deltatime * timeSpeed;
		if (seasonTime >= 360.0f) seasonTime -= 360.0f;
	}
	material.shader->setFloat("seasonTime", GLfloat(glm::radians(seasonTime)));
}

void Terrain::input(int key, int scancode, int action, int mods)
{
	//	Toggle hard boundaries
	if (key == GLFW_KEY_O && action == GLFW_PRESS)
	{
		hardBoundaries = !hardBoundaries;

		material.shader->setInt("hardBoundaries", (GLint)hardBoundaries);
	}
	//	Set spring
	if (key == GLFW_KEY_1 && action == GLFW_PRESS)
	{
		seasonTime = 90.0f;
	}
	//	Set summer
	if (key == GLFW_KEY_2 && action == GLFW_PRESS)
	{
		seasonTime = 180.0f;
	}
	//	Set autumn
	if (key == GLFW_KEY_3 && action == GLFW_PRESS)
	{
		seasonTime = 270.0f;
	}
	//	Set winter
	if (key == GLFW_KEY_4 && action == GLFW_PRESS)
	{
		seasonTime = 0.0f;
	}
	//	Toggle season cycle
	if (key == GLFW_KEY_5 && action == GLFW_PRESS)
	{
		seasonsPaused = !seasonsPaused;
	}
}