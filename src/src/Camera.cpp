#include "src/headers/Camera.h"
#include "src/headers/Globals.h"
#include "src/headers/logger.h"

extern int SCREEN_WIDTH;
extern int SCREEN_HEIGHT;

const float MINFOV = 10.0f;
const float MAXFOV = 75.0f;

Camera::Camera(glm::vec3 pos, glm::vec3 viewDir, glm::vec3 upDir, CameraMode newMode)
{
    transform.position = pos;
    frontDirection = glm::normalize(viewDir);
    upDirection = glm::normalize(upDir);
	rightHandDirection = glm::normalize(glm::cross(frontDirection, glm::vec3(0.0f, 1.0f, 0.0f)));
    mode = newMode;

    rotationAngle = 20.0f;
    rotationSpeed = 1.0f;
	mouseSensitivity = 0.4f;
	pitch = yaw = 0.0f;
    rotationDirection = glm::vec3(0, 1, 0);
    lookatPosition = glm::vec3(0, 0, 0);
    followDistance = glm::vec3(0, 0, 1);
    
	fieldOV = MAXFOV;
    view = glm::mat4(1.0f);
    projection = glm::perspective(glm::radians(fieldOV), float(SCREEN_WIDTH) / float(SCREEN_HEIGHT), 0.1f, 100.0f);
}

void Camera::update()
{
    switch (mode)
    {
    case freeCam:
        view = glm::lookAt(transform.position, transform.position + frontDirection, upDirection);
        view = view * transform.rotation;
        break;

    case staticCam:
        view = glm::lookAt(transform.position, lookatPosition, glm::vec3(0.0f, 1.0f, 0.0f));
        break;

    case followCam:
		transform.position = target->transform.position + followDistance;
        view = glm::lookAt(transform.position, target->transform.position, glm::vec3(0.0f, 1.0f, 0.0f));
        break;

    case trackingCam:
        view = glm::lookAt(transform.position, target->transform.position, glm::vec3(0.0f, 1.0f, 0.0f));
        break;

    case orbitTargetCam:
        // Orbit gameobject
        transform.rotate(rotationAngle, rotationDirection, g_deltatime);
        
        followDistance = transform.rotation * glm::vec4(followDistance, 1.0);
        transform.position = target->transform.position + followDistance;
        view = glm::lookAt(transform.position, target->transform.position, glm::vec3(0.0f, 1.0f, 0.0f));
        break;

    case orbitPositionCam:
		transform.rotate(rotationAngle, rotationDirection, g_deltatime);
        
        followDistance = transform.rotation * glm::vec4(followDistance, 1.0);
        transform.position = lookatPosition + followDistance;
        view = glm::lookAt(transform.position, lookatPosition, glm::vec3(0.0f, 1.0f, 0.0f));
        break;
    }

}

//	Key input specifically, mouse input is further down
void Camera::input(int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_C && action == GLFW_PRESS)
    {
        mode = static_cast<CameraMode>(int(mode) + 1);
        if (int(mode) > 5)
        {
            mode = freeCam;
            transform.position = glm::vec3(0, 10, 5);
            transform.rotation = glm::mat4(1.0f);
        }
        LOG_DEBUG("Cam mode: %d", int(mode));
    }
	if (key == GLFW_KEY_SLASH && action == GLFW_PRESS)	//	GLFW_KEY_SLASH is the minus key on Norwegian keyboards
	{
		mode = followCam;
	}
    if (mode == freeCam)
    {
		//	Move forward
        if (key == GLFW_KEY_I)
        {
            transform.position += frontDirection * 0.25f;
        }
		//	Move backwards
        if (key == GLFW_KEY_K)
        {
            transform.position -= frontDirection * 0.25f;
        }
		//	Move left
        if (key == GLFW_KEY_J)
        {
			transform.position -= rightHandDirection * 0.25f;
        }
		//	Move right
        if (key == GLFW_KEY_L)
        {
			transform.position += rightHandDirection * 0.25f;
        }
		//	Move up
        if (key == GLFW_KEY_Y)
        {
			transform.position += upDirection * 0.25f;
        }
		//	Move down
        if (key == GLFW_KEY_H)
        {
            transform.position -= upDirection * 0.25f;
        }
		//	Zoom in, up to a maximum zoom
		if (key == GLFW_KEY_N)
		{
			setFoV(max(fieldOV - 1.0f, MINFOV));
		}
		//	Zoom out, up to the starting zoom
		if (key == GLFW_KEY_M)
		{
			setFoV(min(fieldOV + 1.0f, MAXFOV));
		}
    }
}

///	Mouse input logic is reused from Assignment 2, which was taken from https://learnopengl.com/Getting-started/Camera
//	Processes input received from a mouse input system. Expects the offset value in both the x and y direction.
void Camera::processMouseMovement(float xoffset, float yoffset, GLboolean constrainPitch)
{
	if (mode == freeCam)
	{
		xoffset *= mouseSensitivity;
		yoffset *= mouseSensitivity;

		yaw += xoffset;
		pitch += yoffset;

		// Make sure that when pitch is out of bounds, screen doesn't get flipped
		if (constrainPitch)
		{
			if (pitch > 89.0f)
				pitch = 89.0f;
			if (pitch < -89.0f)
				pitch = -89.0f;
		}

		// Update Front, Right and Up Vectors using the updated Euler angles
		updateVectors();
	}
}

void Camera::rotate(glm::vec3 dir)
{
    transform.rotation = glm::rotate(glm::mat4(1.0), float(glm::radians(rotationAngle) * g_deltatime * rotationSpeed * 4), dir);
    frontDirection = transform.rotation * glm::vec4(frontDirection, 1.0);
}

void Camera::tilt(glm::vec3 dir)
{
    transform.rotation = glm::rotate(glm::mat4(1.0), float(glm::radians(rotationAngle) * g_deltatime * rotationSpeed * 4), dir);
    upDirection = transform.rotation * glm::vec4(upDirection, 1.0);
}

void Camera::setCamMode(CameraMode newMode)
{
    mode = newMode;
}

void Camera::setFollowDistance(glm::vec3 newDistance)
{
    followDistance = newDistance;
}

void Camera::setUpDirection(glm::vec3 newDirection)
{
    upDirection = newDirection;
}

void Camera::setLookatDirection(glm::vec3 newDirection)
{
    frontDirection = newDirection;
}

void Camera::setLookatPosition(glm::vec3 newPosition)
{
    lookatPosition = newPosition;
}

void Camera::setTarget(GameObject* newTarget)
{
    target = newTarget;
}

void Camera::setRotationDirection(glm::vec3 newDirection)
{
    rotationDirection = newDirection;
}

void Camera::setRotationAngle(float angle)
{
    rotationAngle = angle;
}

void Camera::setRotationSpeed(float speed)
{
    rotationSpeed = speed;
}

void Camera::updateProjection(int width, int height)
{
    projection = glm::perspective(glm::radians(fieldOV), float(width) / float(height), 0.1f, 100.f);
}

void Camera::setFoV(float FoV)
{
	if (FoV >= MINFOV && FoV <= MAXFOV)
	{
		fieldOV = FoV;
		updateProjection(SCREEN_WIDTH, SCREEN_HEIGHT);
	}
	else LOG_DEBUG("Invalid FOV. FOV must be between %f and %f", MINFOV, MAXFOV);
}

float Camera::getFoV()
{
	return fieldOV;
}

glm::vec3 Camera::getFollowDistance()
{
    return followDistance;
}

glm::vec3 Camera::getUpDirection()
{
    return upDirection;
}

glm::vec3 Camera::getFrontDirection()
{
    return frontDirection;
}

glm::vec3 Camera::getLookatPosition()
{
    return lookatPosition;
}

glm::mat4 Camera::getProjection()
{
    return projection;
}

glm::mat4 Camera::getView()
{
    return view;
}

GameObject* Camera::getTarget()
{
    return target;
}