﻿#include "src/headers/Init.h"
#include "src/headers/logger.h"
#include "src/headers/mad.h"
#include "src/headers/Camera.h"
#include "src/headers/ObjectHandler.h"
#include <vector>

#include "glm/glm.hpp"
#include <glm/gtc/matrix_transform.hpp>


#define TINYOBJLOADER_IMPLEMENTATION
#include "src/headers/tiny_obj_loader.h"

extern GLFWwindow* g_window;

extern Camera* mainCamera;

extern int SCREEN_WIDTH;
extern int SCREEN_HEIGHT;

float lastMouseX;
float lastMouseY;
bool firstFrame;

extern glm::mat4 g_projection;
extern glm::mat4 g_view;

bool initWindow()
{
    glfwInit();                                                             // Inits glfw
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);                          // Sets major openGL version
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);                          // Sets minor openGL version
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);          // Sets that we want to use core functionality
    //glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    g_window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "IMT2531 exam 2018, Herman Tandberg Dybing", NULL, NULL);     // Creates a window, size 800x600, Learning 2 as title

    if (g_window == NULL)                                                     // If no window created, terminates program
    {
        LOG_ERROR("Error creating window, terminating...\n");
        glfwTerminate();
        return false;
    }

	firstFrame = true;

    LOG_DEBUG("Window created");

    glfwMakeContextCurrent(g_window);                                       // Sets window to be used
    glfwSetInputMode(g_window, GLFW_STICKY_KEYS, GL_FALSE);
    glfwSetKeyCallback(g_window, inputHandler);
	glfwSetInputMode(g_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glfwSetCursorPosCallback(g_window, mouseCall);
    glewExperimental = GL_TRUE;                                             // For Linux/Mac OS

    if (glewInit() != GLEW_OK)                                              // Inits glew, terminates if not
    {
        LOG_ERROR("Failed to init glew, terminating...\n");
        glfwTerminate();
        return false;
    }

    LOG_DEBUG("Glew initiated");

    glEnable(GL_BLEND);                                                     // Enables use of blending
    glEnable(GL_DEPTH_TEST);                                                // Depth test so that only textures on top get rendered
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);                      // Enables the use of alpha channel
	/*glEnable(GL_CULL_FACE);
	glFrontFace(GL_CCW);
	glCullFace(GL_BACK);*/

    glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);                          // Sets the viewport coordinates and size
    glfwSetFramebufferSizeCallback(g_window, windowsize_callback);          // Sets what function to call when screen gets resized
    glClearColor(0.1f, 0.4f, 0.9f, 1.0f);                                   // Sets a clear color / black here
    glClear(GL_COLOR_BUFFER_BIT);                                           // Clears buffer
    glfwSwapBuffers(g_window);                                              // Swaps buffer
    glClear(GL_COLOR_BUFFER_BIT);                                           // Clears buffer

	TTF_Init();


    LOG_DEBUG("Initialization successful");
    return true;
}

void initTextures()
{

}

void initShaders()
{

}

void initMeshes()
{

}

void initGameObjects()
{

}

void loadSound()
{

}

// Callback function for when the window resizes
void windowsize_callback(GLFWwindow* window, int width, int height)
{

    SCREEN_WIDTH = width;
    SCREEN_HEIGHT = height;

    mainCamera->updateProjection(width, height);
    glfwGetWindowSize(window, &height, &width);
    glViewport(0, 0, height, width);
}

//	Key input handling, not mouse input.
void inputHandler (GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE) glfwSetWindowShouldClose(window, GL_TRUE);
	HandleInput(g_window, key, scancode, action, mods);
	
}

///	Mouse input logic is reused from Assignment 2, which was taken from https://learnopengl.com/Getting-started/Camera
void mouseCall(GLFWwindow * window, double mX, double mY)
{
	if (firstFrame)
	{
		lastMouseX = (float)mX;
		lastMouseY = (float)mY;
		firstFrame = false;
	}
	// mouse offsets
	float dx = (float)mX - lastMouseX;
	float dy = lastMouseY - (float)mY;
	lastMouseX = (float)mX;
	lastMouseY = (float)mY;

	mainCamera->processMouseMovement(dx, dy);
}