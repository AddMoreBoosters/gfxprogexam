#include "src/headers/Sun.h"
#include "src/headers/ObjectHandler.h"
#include "src/headers/Globals.h"

Sun::Sun()
{
	moveAuto = true;
	orbitRadius = 50.0f;
	timeOfDay = 0.0f;
	timeSpeed = 360.0f / 15.0f;			//	360 degrees, divided by how many seconds I want a cycle to take;

	light = CreateLight(transform.position);
}

Sun::~Sun()
{
	delete light;
}

void Sun::update()
{
	if (moveAuto)
	{
		timeOfDay += (float)g_deltatime * timeSpeed;
		if (timeOfDay >= 360.0f) timeOfDay -= 360.0f;
	}
	transform.position = glm::vec3(glm::cos(glm::radians(timeOfDay)) * orbitRadius, glm::sin(glm::radians(timeOfDay)) * orbitRadius, 0);
	light->transform.position = transform.position;
	float lightAngle = (float)glm::dot(glm::normalize(transform.position), glm::vec3(0.0f, 1.0f, 0.0f));
	if (lightAngle < 0.0f) lightAngle = 0.0f;
	light->color = glm::vec3(lightAngle, lightAngle, glm::pow(lightAngle, 2));
}

void Sun::input(int key, int scancode, int action, int mods)
{
	//	Start/stop moving automatically
	if (key == GLFW_KEY_0 && action == GLFW_PRESS)
	{
		moveAuto = !moveAuto;
	}
	//	Dawn
	if (key == GLFW_KEY_6 && action == GLFW_PRESS)
	{
		timeOfDay = 0.0f;
	}
	//	Noon
	if (key == GLFW_KEY_7 && action == GLFW_PRESS)
	{
		timeOfDay = 90.0f;
	}
	//	Dusk
	if (key == GLFW_KEY_8 && action == GLFW_PRESS)
	{
		timeOfDay = 180.0f;
	}
	//	Midnight
	if (key == GLFW_KEY_9 && action == GLFW_PRESS)
	{
		timeOfDay = 270.0f;
	}
}