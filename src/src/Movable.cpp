#pragma once
#include "src/headers/Movable.h"
#include "src/headers/Globals.h"
#include "src/headers/ObjectHandler.h"
#include "src/headers/logger.h"

extern Camera* mainCamera;
float timeSincePrint;

Movable::Movable()
{
    //  Default constructor
	pitch = 0.0f;
	yaw = 0.0f;
	transform.speed = 2.0f;
	fPosition = 0;

	timeSincePrint = 0.0f;
}

void Movable::update()
{
	//	Calculate where the plane is facing, and move in that direction
	glm::vec4 currentFacing = glm::normalize(transform.rotation * glm::vec4(glm::vec3(-1, 0, 0), 1.0));
	glm::vec3 facing = glm::normalize(glm::vec3(currentFacing.x, currentFacing.y, currentFacing.z));
	transform.translate(facing, g_deltatime);

	//	Calculate where the camera should be (behind and a little above), and tell it to be there
	facing *= -4.0f;
	glm::vec3 side = glm::normalize(glm::cross(facing, glm::vec3(0.0f, 1.0f, 0.0f)));
	facing -= 1.5f * glm::normalize(glm::cross(facing, side));
	mainCamera->setFollowDistance(facing);

	//	Provide debug output that lets the user see the current speed
	timeSincePrint += g_deltatime;
	if (timeSincePrint > 1.0f)
	{
		LOG_DEBUG("Current glider speed: %f", transform.speed);
		timeSincePrint -= 1.0f;
	}
}

void Movable::input(int key, int scancode, int action, int mods)
{
	if ( key == GLFW_KEY_W)
    {
        //pitch -= rotationSpeed;
		transform.rotate(30.0f, glm::vec3(0, 0, 1), g_deltatime);
		
    }
    if ( key == GLFW_KEY_S)
    {
        //pitch += rotationSpeed;
		transform.rotate(30.0f, glm::vec3(0, 0, -1), g_deltatime);
    }
    if (key == GLFW_KEY_A)
    {
        //yaw -= rotationSpeed;
		transform.rotate(30.0f, glm::vec3(0, 1, 0), g_deltatime);
    }
    if (key == GLFW_KEY_D)
    {
        //yaw += rotationSpeed;
		transform.rotate(30.0f, glm::vec3(0, -1, 0), g_deltatime);
    }
	if (key == GLFW_KEY_Q)
	{
		//yaw -= rotationSpeed;
		transform.rotate(30.0f, glm::vec3(1, 0, 0), g_deltatime);
	}
	if (key == GLFW_KEY_E)
	{
		//yaw += rotationSpeed;
		transform.rotate(30.0f, glm::vec3(-1, 0, 0), g_deltatime);
	}
	if (key == GLFW_KEY_COMMA)
	{
		transform.speed = max(transform.speed - float(g_deltatime), 0.0f);
	}
	if (key == GLFW_KEY_PERIOD)
	{
		transform.speed = min(transform.speed + float(g_deltatime), 10.0f);
	}
	if (key == GLFW_KEY_R && action == GLFW_PRESS)
	{
		transform.position = glm::vec3(0.0f);
		transform.rotation = glm::mat4(1.0f);
		transform.speed = 2.0f;
	}
	if (key == GLFW_KEY_F && action == GLFW_PRESS)
	{
		switch (fPosition)
		{
		case 0:
			transform.position = glm::vec3(30.0f, 0.0f, 0.0f);
			transform.rotation = glm::mat4(1.0f);
			transform.speed = 2.0f;
			break;
		case 1:
			transform.position = glm::vec3(-30.0f, 0.0f, 0.0f);
			transform.rotation = glm::mat4(1.0f);
			transform.rotate(180.0f, glm::vec3(0.0f, 1.0f, 0.0f), 1.0);
			transform.speed = 2.0f;
			break;
		case 2:
			transform.position = glm::vec3(0.0f, 0.0f, 15.0f);
			transform.rotation = glm::mat4(1.0f);
			transform.rotate(-90.0f, glm::vec3(0.0f, 1.0f, 0.0f), 1.0);
			transform.speed = 2.0f;
			break;
		}
		fPosition = (fPosition + 1) % 3;
	}
}

void Movable::updateVectors()
{
	glm::vec3 front;
	front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
	front.y = sin(glm::radians(pitch));
	front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
	frontDirection = glm::normalize(front);
	rightHandDirection = glm::normalize(glm::cross(frontDirection, glm::vec3(0.0f, 1.0f, 0.0f)));
	upDirection = glm::normalize(glm::cross(rightHandDirection, frontDirection));
}